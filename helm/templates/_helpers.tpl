{{/*
Выводит полное имя приложения
*/}}
{{- define "gdkharit-drweb.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Выводит имя чарта или nameOverride из values.yaml
*/}}
{{- define "gdkharit-drweb.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Выводит неймспейс в зависимости от окружения
*/}}
{{- define "gdkharit-drweb.namespace" }}
{{- if eq .Values.environment "demo" -}}
support-test
{{- end }}
{{- end }}

{{/*
Выводит адрес регистри и имя образа с тегом
*/}}
{{- define "gdkharit-drweb.image" }}
{{- printf  "%s:%s" .Values.image.repository .Values.image.tag }}
{{- end }}

{{/*
Выводит cмонтированные тома
*/}}
{{- define "gdkharit-drweb.mounts" }}
{{- range $claim, $path := .Values.mounts }}
- mountPath: {{ $path | quote }}
  name: {{ $claim }}
{{- end }}
{{- end }}

{{/*
Выводит список томов, которые могут быть смонтированы
*/}}
{{- define "gdkharit-drweb.volumes" }}
{{- range $claim, $path := .Values.mounts }}
- name: {{ $claim }}
  persistentVolumeClaim:
    claimName: {{ $claim }}
{{- end }}
{{- end }}
